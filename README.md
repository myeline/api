# Myeline API

Public gRPC API definition for the Myeline project, and officially supported clients (currently Go and Typescript).

## Documentation

Documentation for the Myeline service can be found [here](./documentation.md).

## Development

### Requirements

Install protoc with:
```shell
brew install protobuf
```

Then download the JS protoc plugin from https://github.com/protocolbuffers/protobuf-javascript/releases and install it with:
```shell
sudo mv ./protobuf-javascript-3.21.2-osx-aarch_64/bin/protoc-gen-js /usr/local/bin/protoc-gen-js
sudo chmod +x /usr/local/bin/protoc-gen-js
```

Finally download the doc protoc plugin from https://github.com/pseudomuto/protoc-gen-doc/releases and install it with:
```shell
sudo mv ./protoc-gen-doc_1.5.1_darwin_arm64/protoc-gen-doc /usr/local/bin/protoc-gen-doc
sudo chmod +x /usr/local/bin/protoc-gen-doc
```

### Generate clients and documentation

```shell
protoc -Iprotos/v1 --go_out=. --go-grpc_out=. --js_out=import_style=commonjs,binary:./libs/v1/ts --grpc-web_out=import_style=typescript,mode=grpcweb:./libs/v1/ts --doc_out=. --doc_opt=markdown,documentation.md ./protos/v1/api.proto
```
