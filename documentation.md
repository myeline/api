# Protocol Documentation
<a name="top"></a>

## Table of Contents

- [api.proto](#api-proto)
    - [Asset](#myeline-api-v1-Asset)
    - [Asset.LabelsEntry](#myeline-api-v1-Asset-LabelsEntry)
    - [CreateProjectRequest](#myeline-api-v1-CreateProjectRequest)
    - [DeleteProjectRequest](#myeline-api-v1-DeleteProjectRequest)
    - [DeleteRetentionPolicyRequest](#myeline-api-v1-DeleteRetentionPolicyRequest)
    - [Event](#myeline-api-v1-Event)
    - [EventsConfiguration](#myeline-api-v1-EventsConfiguration)
    - [GetAssetRequest](#myeline-api-v1-GetAssetRequest)
    - [GetEventsConfigurationRequest](#myeline-api-v1-GetEventsConfigurationRequest)
    - [GetProjectRequest](#myeline-api-v1-GetProjectRequest)
    - [GetRetentionPolicyRequest](#myeline-api-v1-GetRetentionPolicyRequest)
    - [GetVersionRequest](#myeline-api-v1-GetVersionRequest)
    - [ListAssetsRequest](#myeline-api-v1-ListAssetsRequest)
    - [ListAssetsResponse](#myeline-api-v1-ListAssetsResponse)
    - [ListEventsRequest](#myeline-api-v1-ListEventsRequest)
    - [ListEventsResponse](#myeline-api-v1-ListEventsResponse)
    - [ListProjectsRequest](#myeline-api-v1-ListProjectsRequest)
    - [ListProjectsResponse](#myeline-api-v1-ListProjectsResponse)
    - [ListRetentionPoliciesRequest](#myeline-api-v1-ListRetentionPoliciesRequest)
    - [ListRetentionPoliciesResponse](#myeline-api-v1-ListRetentionPoliciesResponse)
    - [Project](#myeline-api-v1-Project)
    - [Project.LabelsEntry](#myeline-api-v1-Project-LabelsEntry)
    - [RegisterAssetRequest](#myeline-api-v1-RegisterAssetRequest)
    - [RetentionPolicy](#myeline-api-v1-RetentionPolicy)
    - [UpdateEventsConfigurationRequest](#myeline-api-v1-UpdateEventsConfigurationRequest)
    - [UpdateProjectRequest](#myeline-api-v1-UpdateProjectRequest)
    - [UpdateRetentionPolicyRequest](#myeline-api-v1-UpdateRetentionPolicyRequest)
    - [Version](#myeline-api-v1-Version)
  
    - [AssetType](#myeline-api-v1-AssetType)
  
    - [Myeline](#myeline-api-v1-Myeline)
  
- [Scalar Value Types](#scalar-value-types)



<a name="api-proto"></a>
<p align="right"><a href="#top">Top</a></p>

## api.proto



<a name="myeline-api-v1-Asset"></a>

### Asset



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| name | [string](#string) |  |  |
| type | [AssetType](#myeline-api-v1-AssetType) |  |  |
| labels | [Asset.LabelsEntry](#myeline-api-v1-Asset-LabelsEntry) | repeated |  |
| attributes | [google.protobuf.Struct](#google-protobuf-Struct) |  |  |
| timestamp | [google.protobuf.Timestamp](#google-protobuf-Timestamp) |  |  |






<a name="myeline-api-v1-Asset-LabelsEntry"></a>

### Asset.LabelsEntry



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| key | [string](#string) |  |  |
| value | [string](#string) |  |  |






<a name="myeline-api-v1-CreateProjectRequest"></a>

### CreateProjectRequest



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| project | [Project](#myeline-api-v1-Project) |  |  |






<a name="myeline-api-v1-DeleteProjectRequest"></a>

### DeleteProjectRequest



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| id | [string](#string) |  |  |






<a name="myeline-api-v1-DeleteRetentionPolicyRequest"></a>

### DeleteRetentionPolicyRequest



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| id | [string](#string) |  |  |






<a name="myeline-api-v1-Event"></a>

### Event



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| timestamp | [google.protobuf.Timestamp](#google-protobuf-Timestamp) |  |  |
| method | [string](#string) |  |  |
| status_code | [int32](#int32) |  |  |
| user_id | [string](#string) |  |  |
| message | [string](#string) |  |  |
| project_id | [string](#string) | optional |  |






<a name="myeline-api-v1-EventsConfiguration"></a>

### EventsConfiguration



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| retention | [google.protobuf.Duration](#google-protobuf-Duration) | optional |  |






<a name="myeline-api-v1-GetAssetRequest"></a>

### GetAssetRequest



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| id | [string](#string) |  |  |






<a name="myeline-api-v1-GetEventsConfigurationRequest"></a>

### GetEventsConfigurationRequest



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| project_id | [string](#string) |  |  |






<a name="myeline-api-v1-GetProjectRequest"></a>

### GetProjectRequest



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| id | [string](#string) |  |  |






<a name="myeline-api-v1-GetRetentionPolicyRequest"></a>

### GetRetentionPolicyRequest



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| id | [string](#string) |  |  |






<a name="myeline-api-v1-GetVersionRequest"></a>

### GetVersionRequest







<a name="myeline-api-v1-ListAssetsRequest"></a>

### ListAssetsRequest



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| project_id | [string](#string) |  |  |
| filter | [string](#string) |  |  |
| start_time | [google.protobuf.Timestamp](#google-protobuf-Timestamp) |  |  |
| end_time | [google.protobuf.Timestamp](#google-protobuf-Timestamp) |  |  |
| page_size | [int32](#int32) |  |  |
| page_token | [string](#string) |  |  |






<a name="myeline-api-v1-ListAssetsResponse"></a>

### ListAssetsResponse



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| assets | [Asset](#myeline-api-v1-Asset) | repeated |  |
| next_page_token | [string](#string) |  |  |






<a name="myeline-api-v1-ListEventsRequest"></a>

### ListEventsRequest



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| project_id | [string](#string) |  |  |
| filter | [string](#string) |  |  |
| start_time | [google.protobuf.Timestamp](#google-protobuf-Timestamp) |  |  |
| end_time | [google.protobuf.Timestamp](#google-protobuf-Timestamp) |  |  |
| page_size | [int32](#int32) |  |  |
| page_token | [string](#string) |  |  |






<a name="myeline-api-v1-ListEventsResponse"></a>

### ListEventsResponse



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| events | [Event](#myeline-api-v1-Event) | repeated |  |
| next_page_token | [string](#string) |  |  |






<a name="myeline-api-v1-ListProjectsRequest"></a>

### ListProjectsRequest



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| filter | [string](#string) |  |  |
| page_size | [int32](#int32) |  |  |
| page_token | [string](#string) |  |  |






<a name="myeline-api-v1-ListProjectsResponse"></a>

### ListProjectsResponse



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| projects | [Project](#myeline-api-v1-Project) | repeated |  |
| next_page_token | [string](#string) |  |  |






<a name="myeline-api-v1-ListRetentionPoliciesRequest"></a>

### ListRetentionPoliciesRequest



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| project_id | [string](#string) |  |  |
| filter | [string](#string) |  |  |
| page_size | [int32](#int32) |  |  |
| page_token | [string](#string) |  |  |






<a name="myeline-api-v1-ListRetentionPoliciesResponse"></a>

### ListRetentionPoliciesResponse



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| retention_policies | [RetentionPolicy](#myeline-api-v1-RetentionPolicy) | repeated |  |
| next_page_token | [string](#string) |  |  |






<a name="myeline-api-v1-Project"></a>

### Project



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| id | [string](#string) |  |  |
| name | [string](#string) |  |  |
| description | [string](#string) |  |  |
| labels | [Project.LabelsEntry](#myeline-api-v1-Project-LabelsEntry) | repeated |  |
| creation_time | [google.protobuf.Timestamp](#google-protobuf-Timestamp) |  |  |






<a name="myeline-api-v1-Project-LabelsEntry"></a>

### Project.LabelsEntry



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| key | [string](#string) |  |  |
| value | [string](#string) |  |  |






<a name="myeline-api-v1-RegisterAssetRequest"></a>

### RegisterAssetRequest



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| project_id | [string](#string) |  |  |
| asset | [Asset](#myeline-api-v1-Asset) |  |  |






<a name="myeline-api-v1-RetentionPolicy"></a>

### RetentionPolicy



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| id | [string](#string) |  |  |
| name | [string](#string) |  |  |
| asset_type | [AssetType](#myeline-api-v1-AssetType) |  |  |
| expiration | [google.protobuf.Duration](#google-protobuf-Duration) | optional |  |
| cooldown | [google.protobuf.Duration](#google-protobuf-Duration) | optional |  |






<a name="myeline-api-v1-UpdateEventsConfigurationRequest"></a>

### UpdateEventsConfigurationRequest



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| project_id | [string](#string) |  |  |
| events_configuration | [EventsConfiguration](#myeline-api-v1-EventsConfiguration) |  |  |
| update_mask | [google.protobuf.FieldMask](#google-protobuf-FieldMask) |  |  |






<a name="myeline-api-v1-UpdateProjectRequest"></a>

### UpdateProjectRequest



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| project | [Project](#myeline-api-v1-Project) |  |  |
| update_mask | [google.protobuf.FieldMask](#google-protobuf-FieldMask) |  |  |






<a name="myeline-api-v1-UpdateRetentionPolicyRequest"></a>

### UpdateRetentionPolicyRequest



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| id | [string](#string) |  |  |
| retention_policy | [RetentionPolicy](#myeline-api-v1-RetentionPolicy) |  |  |
| update_mask | [google.protobuf.FieldMask](#google-protobuf-FieldMask) |  |  |






<a name="myeline-api-v1-Version"></a>

### Version



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| version | [string](#string) |  |  |
| git_commit_hash | [string](#string) |  |  |
| build_time | [google.protobuf.Timestamp](#google-protobuf-Timestamp) |  |  |
| go_version | [string](#string) |  |  |
| os | [string](#string) |  |  |
| arch | [string](#string) |  |  |





 


<a name="myeline-api-v1-AssetType"></a>

### AssetType


| Name | Number | Description |
| ---- | ------ | ----------- |
| ASSET_TYPE_UNSPECIFIED | 0 |  |


 

 


<a name="myeline-api-v1-Myeline"></a>

### Myeline


| Method Name | Request Type | Response Type | Description |
| ----------- | ------------ | ------------- | ------------|
| GetVersion | [GetVersionRequest](#myeline-api-v1-GetVersionRequest) | [Version](#myeline-api-v1-Version) |  |
| ListProjects | [ListProjectsRequest](#myeline-api-v1-ListProjectsRequest) | [ListProjectsResponse](#myeline-api-v1-ListProjectsResponse) |  |
| GetProject | [GetProjectRequest](#myeline-api-v1-GetProjectRequest) | [Project](#myeline-api-v1-Project) |  |
| CreateProject | [CreateProjectRequest](#myeline-api-v1-CreateProjectRequest) | [Project](#myeline-api-v1-Project) |  |
| UpdateProject | [UpdateProjectRequest](#myeline-api-v1-UpdateProjectRequest) | [Project](#myeline-api-v1-Project) |  |
| DeleteProject | [DeleteProjectRequest](#myeline-api-v1-DeleteProjectRequest) | [.google.protobuf.Empty](#google-protobuf-Empty) |  |
| ListAssets | [ListAssetsRequest](#myeline-api-v1-ListAssetsRequest) | [ListAssetsResponse](#myeline-api-v1-ListAssetsResponse) |  |
| GetAsset | [GetAssetRequest](#myeline-api-v1-GetAssetRequest) | [Asset](#myeline-api-v1-Asset) |  |
| RegisterAsset | [RegisterAssetRequest](#myeline-api-v1-RegisterAssetRequest) | [Asset](#myeline-api-v1-Asset) |  |
| ListRetentionPolicies | [ListRetentionPoliciesRequest](#myeline-api-v1-ListRetentionPoliciesRequest) | [ListRetentionPoliciesResponse](#myeline-api-v1-ListRetentionPoliciesResponse) |  |
| GetRetentionPolicy | [GetRetentionPolicyRequest](#myeline-api-v1-GetRetentionPolicyRequest) | [RetentionPolicy](#myeline-api-v1-RetentionPolicy) |  |
| UpdateRetentionPolicy | [UpdateRetentionPolicyRequest](#myeline-api-v1-UpdateRetentionPolicyRequest) | [RetentionPolicy](#myeline-api-v1-RetentionPolicy) |  |
| DeleteRetentionPolicy | [DeleteRetentionPolicyRequest](#myeline-api-v1-DeleteRetentionPolicyRequest) | [.google.protobuf.Empty](#google-protobuf-Empty) |  |
| ListEvents | [ListEventsRequest](#myeline-api-v1-ListEventsRequest) | [ListEventsResponse](#myeline-api-v1-ListEventsResponse) |  |
| GetEventsConfiguration | [GetEventsConfigurationRequest](#myeline-api-v1-GetEventsConfigurationRequest) | [EventsConfiguration](#myeline-api-v1-EventsConfiguration) |  |
| UpdateEventsConfiguration | [UpdateEventsConfigurationRequest](#myeline-api-v1-UpdateEventsConfigurationRequest) | [EventsConfiguration](#myeline-api-v1-EventsConfiguration) |  |

 



## Scalar Value Types

| .proto Type | Notes | C++ | Java | Python | Go | C# | PHP | Ruby |
| ----------- | ----- | --- | ---- | ------ | -- | -- | --- | ---- |
| <a name="double" /> double |  | double | double | float | float64 | double | float | Float |
| <a name="float" /> float |  | float | float | float | float32 | float | float | Float |
| <a name="int32" /> int32 | Uses variable-length encoding. Inefficient for encoding negative numbers – if your field is likely to have negative values, use sint32 instead. | int32 | int | int | int32 | int | integer | Bignum or Fixnum (as required) |
| <a name="int64" /> int64 | Uses variable-length encoding. Inefficient for encoding negative numbers – if your field is likely to have negative values, use sint64 instead. | int64 | long | int/long | int64 | long | integer/string | Bignum |
| <a name="uint32" /> uint32 | Uses variable-length encoding. | uint32 | int | int/long | uint32 | uint | integer | Bignum or Fixnum (as required) |
| <a name="uint64" /> uint64 | Uses variable-length encoding. | uint64 | long | int/long | uint64 | ulong | integer/string | Bignum or Fixnum (as required) |
| <a name="sint32" /> sint32 | Uses variable-length encoding. Signed int value. These more efficiently encode negative numbers than regular int32s. | int32 | int | int | int32 | int | integer | Bignum or Fixnum (as required) |
| <a name="sint64" /> sint64 | Uses variable-length encoding. Signed int value. These more efficiently encode negative numbers than regular int64s. | int64 | long | int/long | int64 | long | integer/string | Bignum |
| <a name="fixed32" /> fixed32 | Always four bytes. More efficient than uint32 if values are often greater than 2^28. | uint32 | int | int | uint32 | uint | integer | Bignum or Fixnum (as required) |
| <a name="fixed64" /> fixed64 | Always eight bytes. More efficient than uint64 if values are often greater than 2^56. | uint64 | long | int/long | uint64 | ulong | integer/string | Bignum |
| <a name="sfixed32" /> sfixed32 | Always four bytes. | int32 | int | int | int32 | int | integer | Bignum or Fixnum (as required) |
| <a name="sfixed64" /> sfixed64 | Always eight bytes. | int64 | long | int/long | int64 | long | integer/string | Bignum |
| <a name="bool" /> bool |  | bool | boolean | boolean | bool | bool | boolean | TrueClass/FalseClass |
| <a name="string" /> string | A string must always contain UTF-8 encoded or 7-bit ASCII text. | string | String | str/unicode | string | string | string | String (UTF-8) |
| <a name="bytes" /> bytes | May contain any arbitrary sequence of bytes. | string | ByteString | str | []byte | ByteString | string | String (ASCII-8BIT) |

