import * as jspb from 'google-protobuf'

import * as google_protobuf_timestamp_pb from 'google-protobuf/google/protobuf/timestamp_pb';
import * as google_protobuf_duration_pb from 'google-protobuf/google/protobuf/duration_pb';
import * as google_protobuf_field_mask_pb from 'google-protobuf/google/protobuf/field_mask_pb';
import * as google_protobuf_empty_pb from 'google-protobuf/google/protobuf/empty_pb';
import * as google_protobuf_struct_pb from 'google-protobuf/google/protobuf/struct_pb';


export class GetVersionRequest extends jspb.Message {
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetVersionRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetVersionRequest): GetVersionRequest.AsObject;
  static serializeBinaryToWriter(message: GetVersionRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetVersionRequest;
  static deserializeBinaryFromReader(message: GetVersionRequest, reader: jspb.BinaryReader): GetVersionRequest;
}

export namespace GetVersionRequest {
  export type AsObject = {
  }
}

export class Version extends jspb.Message {
  getVersion(): string;
  setVersion(value: string): Version;

  getGitCommitHash(): string;
  setGitCommitHash(value: string): Version;

  getBuildTime(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setBuildTime(value?: google_protobuf_timestamp_pb.Timestamp): Version;
  hasBuildTime(): boolean;
  clearBuildTime(): Version;

  getGoVersion(): string;
  setGoVersion(value: string): Version;

  getOs(): string;
  setOs(value: string): Version;

  getArch(): string;
  setArch(value: string): Version;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Version.AsObject;
  static toObject(includeInstance: boolean, msg: Version): Version.AsObject;
  static serializeBinaryToWriter(message: Version, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Version;
  static deserializeBinaryFromReader(message: Version, reader: jspb.BinaryReader): Version;
}

export namespace Version {
  export type AsObject = {
    version: string,
    gitCommitHash: string,
    buildTime?: google_protobuf_timestamp_pb.Timestamp.AsObject,
    goVersion: string,
    os: string,
    arch: string,
  }
}

export class ListProjectsRequest extends jspb.Message {
  getFilter(): string;
  setFilter(value: string): ListProjectsRequest;

  getPageSize(): number;
  setPageSize(value: number): ListProjectsRequest;

  getPageToken(): string;
  setPageToken(value: string): ListProjectsRequest;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListProjectsRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListProjectsRequest): ListProjectsRequest.AsObject;
  static serializeBinaryToWriter(message: ListProjectsRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListProjectsRequest;
  static deserializeBinaryFromReader(message: ListProjectsRequest, reader: jspb.BinaryReader): ListProjectsRequest;
}

export namespace ListProjectsRequest {
  export type AsObject = {
    filter: string,
    pageSize: number,
    pageToken: string,
  }
}

export class ListProjectsResponse extends jspb.Message {
  getProjectsList(): Array<Project>;
  setProjectsList(value: Array<Project>): ListProjectsResponse;
  clearProjectsList(): ListProjectsResponse;
  addProjects(value?: Project, index?: number): Project;

  getNextPageToken(): string;
  setNextPageToken(value: string): ListProjectsResponse;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListProjectsResponse.AsObject;
  static toObject(includeInstance: boolean, msg: ListProjectsResponse): ListProjectsResponse.AsObject;
  static serializeBinaryToWriter(message: ListProjectsResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListProjectsResponse;
  static deserializeBinaryFromReader(message: ListProjectsResponse, reader: jspb.BinaryReader): ListProjectsResponse;
}

export namespace ListProjectsResponse {
  export type AsObject = {
    projectsList: Array<Project.AsObject>,
    nextPageToken: string,
  }
}

export class GetProjectRequest extends jspb.Message {
  getId(): string;
  setId(value: string): GetProjectRequest;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetProjectRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetProjectRequest): GetProjectRequest.AsObject;
  static serializeBinaryToWriter(message: GetProjectRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetProjectRequest;
  static deserializeBinaryFromReader(message: GetProjectRequest, reader: jspb.BinaryReader): GetProjectRequest;
}

export namespace GetProjectRequest {
  export type AsObject = {
    id: string,
  }
}

export class CreateProjectRequest extends jspb.Message {
  getProject(): Project | undefined;
  setProject(value?: Project): CreateProjectRequest;
  hasProject(): boolean;
  clearProject(): CreateProjectRequest;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): CreateProjectRequest.AsObject;
  static toObject(includeInstance: boolean, msg: CreateProjectRequest): CreateProjectRequest.AsObject;
  static serializeBinaryToWriter(message: CreateProjectRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): CreateProjectRequest;
  static deserializeBinaryFromReader(message: CreateProjectRequest, reader: jspb.BinaryReader): CreateProjectRequest;
}

export namespace CreateProjectRequest {
  export type AsObject = {
    project?: Project.AsObject,
  }
}

export class UpdateProjectRequest extends jspb.Message {
  getProject(): Project | undefined;
  setProject(value?: Project): UpdateProjectRequest;
  hasProject(): boolean;
  clearProject(): UpdateProjectRequest;

  getUpdateMask(): google_protobuf_field_mask_pb.FieldMask | undefined;
  setUpdateMask(value?: google_protobuf_field_mask_pb.FieldMask): UpdateProjectRequest;
  hasUpdateMask(): boolean;
  clearUpdateMask(): UpdateProjectRequest;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): UpdateProjectRequest.AsObject;
  static toObject(includeInstance: boolean, msg: UpdateProjectRequest): UpdateProjectRequest.AsObject;
  static serializeBinaryToWriter(message: UpdateProjectRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): UpdateProjectRequest;
  static deserializeBinaryFromReader(message: UpdateProjectRequest, reader: jspb.BinaryReader): UpdateProjectRequest;
}

export namespace UpdateProjectRequest {
  export type AsObject = {
    project?: Project.AsObject,
    updateMask?: google_protobuf_field_mask_pb.FieldMask.AsObject,
  }
}

export class DeleteProjectRequest extends jspb.Message {
  getId(): string;
  setId(value: string): DeleteProjectRequest;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): DeleteProjectRequest.AsObject;
  static toObject(includeInstance: boolean, msg: DeleteProjectRequest): DeleteProjectRequest.AsObject;
  static serializeBinaryToWriter(message: DeleteProjectRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): DeleteProjectRequest;
  static deserializeBinaryFromReader(message: DeleteProjectRequest, reader: jspb.BinaryReader): DeleteProjectRequest;
}

export namespace DeleteProjectRequest {
  export type AsObject = {
    id: string,
  }
}

export class Project extends jspb.Message {
  getId(): string;
  setId(value: string): Project;

  getName(): string;
  setName(value: string): Project;

  getDescription(): string;
  setDescription(value: string): Project;

  getLabelsMap(): jspb.Map<string, string>;
  clearLabelsMap(): Project;

  getCreationTime(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setCreationTime(value?: google_protobuf_timestamp_pb.Timestamp): Project;
  hasCreationTime(): boolean;
  clearCreationTime(): Project;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Project.AsObject;
  static toObject(includeInstance: boolean, msg: Project): Project.AsObject;
  static serializeBinaryToWriter(message: Project, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Project;
  static deserializeBinaryFromReader(message: Project, reader: jspb.BinaryReader): Project;
}

export namespace Project {
  export type AsObject = {
    id: string,
    name: string,
    description: string,
    labelsMap: Array<[string, string]>,
    creationTime?: google_protobuf_timestamp_pb.Timestamp.AsObject,
  }
}

export class ListAssetsRequest extends jspb.Message {
  getProjectId(): string;
  setProjectId(value: string): ListAssetsRequest;

  getFilter(): string;
  setFilter(value: string): ListAssetsRequest;

  getStartTime(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setStartTime(value?: google_protobuf_timestamp_pb.Timestamp): ListAssetsRequest;
  hasStartTime(): boolean;
  clearStartTime(): ListAssetsRequest;

  getEndTime(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setEndTime(value?: google_protobuf_timestamp_pb.Timestamp): ListAssetsRequest;
  hasEndTime(): boolean;
  clearEndTime(): ListAssetsRequest;

  getPageSize(): number;
  setPageSize(value: number): ListAssetsRequest;

  getPageToken(): string;
  setPageToken(value: string): ListAssetsRequest;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListAssetsRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListAssetsRequest): ListAssetsRequest.AsObject;
  static serializeBinaryToWriter(message: ListAssetsRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListAssetsRequest;
  static deserializeBinaryFromReader(message: ListAssetsRequest, reader: jspb.BinaryReader): ListAssetsRequest;
}

export namespace ListAssetsRequest {
  export type AsObject = {
    projectId: string,
    filter: string,
    startTime?: google_protobuf_timestamp_pb.Timestamp.AsObject,
    endTime?: google_protobuf_timestamp_pb.Timestamp.AsObject,
    pageSize: number,
    pageToken: string,
  }
}

export class ListAssetsResponse extends jspb.Message {
  getAssetsList(): Array<Asset>;
  setAssetsList(value: Array<Asset>): ListAssetsResponse;
  clearAssetsList(): ListAssetsResponse;
  addAssets(value?: Asset, index?: number): Asset;

  getNextPageToken(): string;
  setNextPageToken(value: string): ListAssetsResponse;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListAssetsResponse.AsObject;
  static toObject(includeInstance: boolean, msg: ListAssetsResponse): ListAssetsResponse.AsObject;
  static serializeBinaryToWriter(message: ListAssetsResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListAssetsResponse;
  static deserializeBinaryFromReader(message: ListAssetsResponse, reader: jspb.BinaryReader): ListAssetsResponse;
}

export namespace ListAssetsResponse {
  export type AsObject = {
    assetsList: Array<Asset.AsObject>,
    nextPageToken: string,
  }
}

export class RegisterAssetRequest extends jspb.Message {
  getProjectId(): string;
  setProjectId(value: string): RegisterAssetRequest;

  getAsset(): Asset | undefined;
  setAsset(value?: Asset): RegisterAssetRequest;
  hasAsset(): boolean;
  clearAsset(): RegisterAssetRequest;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): RegisterAssetRequest.AsObject;
  static toObject(includeInstance: boolean, msg: RegisterAssetRequest): RegisterAssetRequest.AsObject;
  static serializeBinaryToWriter(message: RegisterAssetRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): RegisterAssetRequest;
  static deserializeBinaryFromReader(message: RegisterAssetRequest, reader: jspb.BinaryReader): RegisterAssetRequest;
}

export namespace RegisterAssetRequest {
  export type AsObject = {
    projectId: string,
    asset?: Asset.AsObject,
  }
}

export class GetAssetRequest extends jspb.Message {
  getId(): string;
  setId(value: string): GetAssetRequest;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetAssetRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetAssetRequest): GetAssetRequest.AsObject;
  static serializeBinaryToWriter(message: GetAssetRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetAssetRequest;
  static deserializeBinaryFromReader(message: GetAssetRequest, reader: jspb.BinaryReader): GetAssetRequest;
}

export namespace GetAssetRequest {
  export type AsObject = {
    id: string,
  }
}

export class Asset extends jspb.Message {
  getName(): string;
  setName(value: string): Asset;

  getType(): AssetType;
  setType(value: AssetType): Asset;

  getLabelsMap(): jspb.Map<string, string>;
  clearLabelsMap(): Asset;

  getAttributes(): google_protobuf_struct_pb.Struct | undefined;
  setAttributes(value?: google_protobuf_struct_pb.Struct): Asset;
  hasAttributes(): boolean;
  clearAttributes(): Asset;

  getTimestamp(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setTimestamp(value?: google_protobuf_timestamp_pb.Timestamp): Asset;
  hasTimestamp(): boolean;
  clearTimestamp(): Asset;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Asset.AsObject;
  static toObject(includeInstance: boolean, msg: Asset): Asset.AsObject;
  static serializeBinaryToWriter(message: Asset, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Asset;
  static deserializeBinaryFromReader(message: Asset, reader: jspb.BinaryReader): Asset;
}

export namespace Asset {
  export type AsObject = {
    name: string,
    type: AssetType,
    labelsMap: Array<[string, string]>,
    attributes?: google_protobuf_struct_pb.Struct.AsObject,
    timestamp?: google_protobuf_timestamp_pb.Timestamp.AsObject,
  }
}

export class ListRetentionPoliciesRequest extends jspb.Message {
  getProjectId(): string;
  setProjectId(value: string): ListRetentionPoliciesRequest;

  getFilter(): string;
  setFilter(value: string): ListRetentionPoliciesRequest;

  getPageSize(): number;
  setPageSize(value: number): ListRetentionPoliciesRequest;

  getPageToken(): string;
  setPageToken(value: string): ListRetentionPoliciesRequest;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListRetentionPoliciesRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListRetentionPoliciesRequest): ListRetentionPoliciesRequest.AsObject;
  static serializeBinaryToWriter(message: ListRetentionPoliciesRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListRetentionPoliciesRequest;
  static deserializeBinaryFromReader(message: ListRetentionPoliciesRequest, reader: jspb.BinaryReader): ListRetentionPoliciesRequest;
}

export namespace ListRetentionPoliciesRequest {
  export type AsObject = {
    projectId: string,
    filter: string,
    pageSize: number,
    pageToken: string,
  }
}

export class ListRetentionPoliciesResponse extends jspb.Message {
  getRetentionPoliciesList(): Array<RetentionPolicy>;
  setRetentionPoliciesList(value: Array<RetentionPolicy>): ListRetentionPoliciesResponse;
  clearRetentionPoliciesList(): ListRetentionPoliciesResponse;
  addRetentionPolicies(value?: RetentionPolicy, index?: number): RetentionPolicy;

  getNextPageToken(): string;
  setNextPageToken(value: string): ListRetentionPoliciesResponse;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListRetentionPoliciesResponse.AsObject;
  static toObject(includeInstance: boolean, msg: ListRetentionPoliciesResponse): ListRetentionPoliciesResponse.AsObject;
  static serializeBinaryToWriter(message: ListRetentionPoliciesResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListRetentionPoliciesResponse;
  static deserializeBinaryFromReader(message: ListRetentionPoliciesResponse, reader: jspb.BinaryReader): ListRetentionPoliciesResponse;
}

export namespace ListRetentionPoliciesResponse {
  export type AsObject = {
    retentionPoliciesList: Array<RetentionPolicy.AsObject>,
    nextPageToken: string,
  }
}

export class GetRetentionPolicyRequest extends jspb.Message {
  getId(): string;
  setId(value: string): GetRetentionPolicyRequest;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetRetentionPolicyRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetRetentionPolicyRequest): GetRetentionPolicyRequest.AsObject;
  static serializeBinaryToWriter(message: GetRetentionPolicyRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetRetentionPolicyRequest;
  static deserializeBinaryFromReader(message: GetRetentionPolicyRequest, reader: jspb.BinaryReader): GetRetentionPolicyRequest;
}

export namespace GetRetentionPolicyRequest {
  export type AsObject = {
    id: string,
  }
}

export class UpdateRetentionPolicyRequest extends jspb.Message {
  getId(): string;
  setId(value: string): UpdateRetentionPolicyRequest;

  getRetentionPolicy(): RetentionPolicy | undefined;
  setRetentionPolicy(value?: RetentionPolicy): UpdateRetentionPolicyRequest;
  hasRetentionPolicy(): boolean;
  clearRetentionPolicy(): UpdateRetentionPolicyRequest;

  getUpdateMask(): google_protobuf_field_mask_pb.FieldMask | undefined;
  setUpdateMask(value?: google_protobuf_field_mask_pb.FieldMask): UpdateRetentionPolicyRequest;
  hasUpdateMask(): boolean;
  clearUpdateMask(): UpdateRetentionPolicyRequest;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): UpdateRetentionPolicyRequest.AsObject;
  static toObject(includeInstance: boolean, msg: UpdateRetentionPolicyRequest): UpdateRetentionPolicyRequest.AsObject;
  static serializeBinaryToWriter(message: UpdateRetentionPolicyRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): UpdateRetentionPolicyRequest;
  static deserializeBinaryFromReader(message: UpdateRetentionPolicyRequest, reader: jspb.BinaryReader): UpdateRetentionPolicyRequest;
}

export namespace UpdateRetentionPolicyRequest {
  export type AsObject = {
    id: string,
    retentionPolicy?: RetentionPolicy.AsObject,
    updateMask?: google_protobuf_field_mask_pb.FieldMask.AsObject,
  }
}

export class DeleteRetentionPolicyRequest extends jspb.Message {
  getId(): string;
  setId(value: string): DeleteRetentionPolicyRequest;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): DeleteRetentionPolicyRequest.AsObject;
  static toObject(includeInstance: boolean, msg: DeleteRetentionPolicyRequest): DeleteRetentionPolicyRequest.AsObject;
  static serializeBinaryToWriter(message: DeleteRetentionPolicyRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): DeleteRetentionPolicyRequest;
  static deserializeBinaryFromReader(message: DeleteRetentionPolicyRequest, reader: jspb.BinaryReader): DeleteRetentionPolicyRequest;
}

export namespace DeleteRetentionPolicyRequest {
  export type AsObject = {
    id: string,
  }
}

export class RetentionPolicy extends jspb.Message {
  getId(): string;
  setId(value: string): RetentionPolicy;

  getName(): string;
  setName(value: string): RetentionPolicy;

  getAssetType(): AssetType;
  setAssetType(value: AssetType): RetentionPolicy;

  getExpiration(): google_protobuf_duration_pb.Duration | undefined;
  setExpiration(value?: google_protobuf_duration_pb.Duration): RetentionPolicy;
  hasExpiration(): boolean;
  clearExpiration(): RetentionPolicy;

  getCooldown(): google_protobuf_duration_pb.Duration | undefined;
  setCooldown(value?: google_protobuf_duration_pb.Duration): RetentionPolicy;
  hasCooldown(): boolean;
  clearCooldown(): RetentionPolicy;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): RetentionPolicy.AsObject;
  static toObject(includeInstance: boolean, msg: RetentionPolicy): RetentionPolicy.AsObject;
  static serializeBinaryToWriter(message: RetentionPolicy, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): RetentionPolicy;
  static deserializeBinaryFromReader(message: RetentionPolicy, reader: jspb.BinaryReader): RetentionPolicy;
}

export namespace RetentionPolicy {
  export type AsObject = {
    id: string,
    name: string,
    assetType: AssetType,
    expiration?: google_protobuf_duration_pb.Duration.AsObject,
    cooldown?: google_protobuf_duration_pb.Duration.AsObject,
  }

  export enum ExpirationCase { 
    _EXPIRATION_NOT_SET = 0,
    EXPIRATION = 4,
  }

  export enum CooldownCase { 
    _COOLDOWN_NOT_SET = 0,
    COOLDOWN = 5,
  }
}

export class ListEventsRequest extends jspb.Message {
  getProjectId(): string;
  setProjectId(value: string): ListEventsRequest;

  getFilter(): string;
  setFilter(value: string): ListEventsRequest;

  getStartTime(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setStartTime(value?: google_protobuf_timestamp_pb.Timestamp): ListEventsRequest;
  hasStartTime(): boolean;
  clearStartTime(): ListEventsRequest;

  getEndTime(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setEndTime(value?: google_protobuf_timestamp_pb.Timestamp): ListEventsRequest;
  hasEndTime(): boolean;
  clearEndTime(): ListEventsRequest;

  getPageSize(): number;
  setPageSize(value: number): ListEventsRequest;

  getPageToken(): string;
  setPageToken(value: string): ListEventsRequest;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListEventsRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListEventsRequest): ListEventsRequest.AsObject;
  static serializeBinaryToWriter(message: ListEventsRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListEventsRequest;
  static deserializeBinaryFromReader(message: ListEventsRequest, reader: jspb.BinaryReader): ListEventsRequest;
}

export namespace ListEventsRequest {
  export type AsObject = {
    projectId: string,
    filter: string,
    startTime?: google_protobuf_timestamp_pb.Timestamp.AsObject,
    endTime?: google_protobuf_timestamp_pb.Timestamp.AsObject,
    pageSize: number,
    pageToken: string,
  }
}

export class ListEventsResponse extends jspb.Message {
  getEventsList(): Array<Event>;
  setEventsList(value: Array<Event>): ListEventsResponse;
  clearEventsList(): ListEventsResponse;
  addEvents(value?: Event, index?: number): Event;

  getNextPageToken(): string;
  setNextPageToken(value: string): ListEventsResponse;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListEventsResponse.AsObject;
  static toObject(includeInstance: boolean, msg: ListEventsResponse): ListEventsResponse.AsObject;
  static serializeBinaryToWriter(message: ListEventsResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListEventsResponse;
  static deserializeBinaryFromReader(message: ListEventsResponse, reader: jspb.BinaryReader): ListEventsResponse;
}

export namespace ListEventsResponse {
  export type AsObject = {
    eventsList: Array<Event.AsObject>,
    nextPageToken: string,
  }
}

export class Event extends jspb.Message {
  getTimestamp(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setTimestamp(value?: google_protobuf_timestamp_pb.Timestamp): Event;
  hasTimestamp(): boolean;
  clearTimestamp(): Event;

  getMethod(): string;
  setMethod(value: string): Event;

  getStatusCode(): number;
  setStatusCode(value: number): Event;

  getUserId(): string;
  setUserId(value: string): Event;

  getMessage(): string;
  setMessage(value: string): Event;

  getProjectId(): string;
  setProjectId(value: string): Event;
  hasProjectId(): boolean;
  clearProjectId(): Event;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Event.AsObject;
  static toObject(includeInstance: boolean, msg: Event): Event.AsObject;
  static serializeBinaryToWriter(message: Event, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Event;
  static deserializeBinaryFromReader(message: Event, reader: jspb.BinaryReader): Event;
}

export namespace Event {
  export type AsObject = {
    timestamp?: google_protobuf_timestamp_pb.Timestamp.AsObject,
    method: string,
    statusCode: number,
    userId: string,
    message: string,
    projectId?: string,
  }

  export enum ProjectIdCase { 
    _PROJECT_ID_NOT_SET = 0,
    PROJECT_ID = 6,
  }
}

export class GetEventsConfigurationRequest extends jspb.Message {
  getProjectId(): string;
  setProjectId(value: string): GetEventsConfigurationRequest;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetEventsConfigurationRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetEventsConfigurationRequest): GetEventsConfigurationRequest.AsObject;
  static serializeBinaryToWriter(message: GetEventsConfigurationRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetEventsConfigurationRequest;
  static deserializeBinaryFromReader(message: GetEventsConfigurationRequest, reader: jspb.BinaryReader): GetEventsConfigurationRequest;
}

export namespace GetEventsConfigurationRequest {
  export type AsObject = {
    projectId: string,
  }
}

export class UpdateEventsConfigurationRequest extends jspb.Message {
  getProjectId(): string;
  setProjectId(value: string): UpdateEventsConfigurationRequest;

  getEventsConfiguration(): EventsConfiguration | undefined;
  setEventsConfiguration(value?: EventsConfiguration): UpdateEventsConfigurationRequest;
  hasEventsConfiguration(): boolean;
  clearEventsConfiguration(): UpdateEventsConfigurationRequest;

  getUpdateMask(): google_protobuf_field_mask_pb.FieldMask | undefined;
  setUpdateMask(value?: google_protobuf_field_mask_pb.FieldMask): UpdateEventsConfigurationRequest;
  hasUpdateMask(): boolean;
  clearUpdateMask(): UpdateEventsConfigurationRequest;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): UpdateEventsConfigurationRequest.AsObject;
  static toObject(includeInstance: boolean, msg: UpdateEventsConfigurationRequest): UpdateEventsConfigurationRequest.AsObject;
  static serializeBinaryToWriter(message: UpdateEventsConfigurationRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): UpdateEventsConfigurationRequest;
  static deserializeBinaryFromReader(message: UpdateEventsConfigurationRequest, reader: jspb.BinaryReader): UpdateEventsConfigurationRequest;
}

export namespace UpdateEventsConfigurationRequest {
  export type AsObject = {
    projectId: string,
    eventsConfiguration?: EventsConfiguration.AsObject,
    updateMask?: google_protobuf_field_mask_pb.FieldMask.AsObject,
  }
}

export class EventsConfiguration extends jspb.Message {
  getRetention(): google_protobuf_duration_pb.Duration | undefined;
  setRetention(value?: google_protobuf_duration_pb.Duration): EventsConfiguration;
  hasRetention(): boolean;
  clearRetention(): EventsConfiguration;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): EventsConfiguration.AsObject;
  static toObject(includeInstance: boolean, msg: EventsConfiguration): EventsConfiguration.AsObject;
  static serializeBinaryToWriter(message: EventsConfiguration, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): EventsConfiguration;
  static deserializeBinaryFromReader(message: EventsConfiguration, reader: jspb.BinaryReader): EventsConfiguration;
}

export namespace EventsConfiguration {
  export type AsObject = {
    retention?: google_protobuf_duration_pb.Duration.AsObject,
  }

  export enum RetentionCase { 
    _RETENTION_NOT_SET = 0,
    RETENTION = 1,
  }
}

export enum AssetType { 
  ASSET_TYPE_UNSPECIFIED = 0,
}
