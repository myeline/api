// Code generated by protoc-gen-go-grpc. DO NOT EDIT.
// versions:
// - protoc-gen-go-grpc v1.2.0
// - protoc             v4.25.2
// source: api.proto

package _go

import (
	context "context"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
	emptypb "google.golang.org/protobuf/types/known/emptypb"
)

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
// Requires gRPC-Go v1.32.0 or later.
const _ = grpc.SupportPackageIsVersion7

// MyelineClient is the client API for Myeline service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://pkg.go.dev/google.golang.org/grpc/?tab=doc#ClientConn.NewStream.
type MyelineClient interface {
	GetVersion(ctx context.Context, in *GetVersionRequest, opts ...grpc.CallOption) (*Version, error)
	ListProjects(ctx context.Context, in *ListProjectsRequest, opts ...grpc.CallOption) (*ListProjectsResponse, error)
	GetProject(ctx context.Context, in *GetProjectRequest, opts ...grpc.CallOption) (*Project, error)
	CreateProject(ctx context.Context, in *CreateProjectRequest, opts ...grpc.CallOption) (*Project, error)
	UpdateProject(ctx context.Context, in *UpdateProjectRequest, opts ...grpc.CallOption) (*Project, error)
	DeleteProject(ctx context.Context, in *DeleteProjectRequest, opts ...grpc.CallOption) (*emptypb.Empty, error)
	ListAssets(ctx context.Context, in *ListAssetsRequest, opts ...grpc.CallOption) (*ListAssetsResponse, error)
	GetAsset(ctx context.Context, in *GetAssetRequest, opts ...grpc.CallOption) (*Asset, error)
	RegisterAsset(ctx context.Context, in *RegisterAssetRequest, opts ...grpc.CallOption) (*Asset, error)
	ListRetentionPolicies(ctx context.Context, in *ListRetentionPoliciesRequest, opts ...grpc.CallOption) (*ListRetentionPoliciesResponse, error)
	GetRetentionPolicy(ctx context.Context, in *GetRetentionPolicyRequest, opts ...grpc.CallOption) (*RetentionPolicy, error)
	UpdateRetentionPolicy(ctx context.Context, in *UpdateRetentionPolicyRequest, opts ...grpc.CallOption) (*RetentionPolicy, error)
	DeleteRetentionPolicy(ctx context.Context, in *DeleteRetentionPolicyRequest, opts ...grpc.CallOption) (*emptypb.Empty, error)
	ListEvents(ctx context.Context, in *ListEventsRequest, opts ...grpc.CallOption) (*ListEventsResponse, error)
	GetEventsConfiguration(ctx context.Context, in *GetEventsConfigurationRequest, opts ...grpc.CallOption) (*EventsConfiguration, error)
	UpdateEventsConfiguration(ctx context.Context, in *UpdateEventsConfigurationRequest, opts ...grpc.CallOption) (*EventsConfiguration, error)
}

type myelineClient struct {
	cc grpc.ClientConnInterface
}

func NewMyelineClient(cc grpc.ClientConnInterface) MyelineClient {
	return &myelineClient{cc}
}

func (c *myelineClient) GetVersion(ctx context.Context, in *GetVersionRequest, opts ...grpc.CallOption) (*Version, error) {
	out := new(Version)
	err := c.cc.Invoke(ctx, "/myeline.api.v1.Myeline/GetVersion", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *myelineClient) ListProjects(ctx context.Context, in *ListProjectsRequest, opts ...grpc.CallOption) (*ListProjectsResponse, error) {
	out := new(ListProjectsResponse)
	err := c.cc.Invoke(ctx, "/myeline.api.v1.Myeline/ListProjects", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *myelineClient) GetProject(ctx context.Context, in *GetProjectRequest, opts ...grpc.CallOption) (*Project, error) {
	out := new(Project)
	err := c.cc.Invoke(ctx, "/myeline.api.v1.Myeline/GetProject", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *myelineClient) CreateProject(ctx context.Context, in *CreateProjectRequest, opts ...grpc.CallOption) (*Project, error) {
	out := new(Project)
	err := c.cc.Invoke(ctx, "/myeline.api.v1.Myeline/CreateProject", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *myelineClient) UpdateProject(ctx context.Context, in *UpdateProjectRequest, opts ...grpc.CallOption) (*Project, error) {
	out := new(Project)
	err := c.cc.Invoke(ctx, "/myeline.api.v1.Myeline/UpdateProject", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *myelineClient) DeleteProject(ctx context.Context, in *DeleteProjectRequest, opts ...grpc.CallOption) (*emptypb.Empty, error) {
	out := new(emptypb.Empty)
	err := c.cc.Invoke(ctx, "/myeline.api.v1.Myeline/DeleteProject", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *myelineClient) ListAssets(ctx context.Context, in *ListAssetsRequest, opts ...grpc.CallOption) (*ListAssetsResponse, error) {
	out := new(ListAssetsResponse)
	err := c.cc.Invoke(ctx, "/myeline.api.v1.Myeline/ListAssets", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *myelineClient) GetAsset(ctx context.Context, in *GetAssetRequest, opts ...grpc.CallOption) (*Asset, error) {
	out := new(Asset)
	err := c.cc.Invoke(ctx, "/myeline.api.v1.Myeline/GetAsset", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *myelineClient) RegisterAsset(ctx context.Context, in *RegisterAssetRequest, opts ...grpc.CallOption) (*Asset, error) {
	out := new(Asset)
	err := c.cc.Invoke(ctx, "/myeline.api.v1.Myeline/RegisterAsset", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *myelineClient) ListRetentionPolicies(ctx context.Context, in *ListRetentionPoliciesRequest, opts ...grpc.CallOption) (*ListRetentionPoliciesResponse, error) {
	out := new(ListRetentionPoliciesResponse)
	err := c.cc.Invoke(ctx, "/myeline.api.v1.Myeline/ListRetentionPolicies", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *myelineClient) GetRetentionPolicy(ctx context.Context, in *GetRetentionPolicyRequest, opts ...grpc.CallOption) (*RetentionPolicy, error) {
	out := new(RetentionPolicy)
	err := c.cc.Invoke(ctx, "/myeline.api.v1.Myeline/GetRetentionPolicy", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *myelineClient) UpdateRetentionPolicy(ctx context.Context, in *UpdateRetentionPolicyRequest, opts ...grpc.CallOption) (*RetentionPolicy, error) {
	out := new(RetentionPolicy)
	err := c.cc.Invoke(ctx, "/myeline.api.v1.Myeline/UpdateRetentionPolicy", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *myelineClient) DeleteRetentionPolicy(ctx context.Context, in *DeleteRetentionPolicyRequest, opts ...grpc.CallOption) (*emptypb.Empty, error) {
	out := new(emptypb.Empty)
	err := c.cc.Invoke(ctx, "/myeline.api.v1.Myeline/DeleteRetentionPolicy", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *myelineClient) ListEvents(ctx context.Context, in *ListEventsRequest, opts ...grpc.CallOption) (*ListEventsResponse, error) {
	out := new(ListEventsResponse)
	err := c.cc.Invoke(ctx, "/myeline.api.v1.Myeline/ListEvents", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *myelineClient) GetEventsConfiguration(ctx context.Context, in *GetEventsConfigurationRequest, opts ...grpc.CallOption) (*EventsConfiguration, error) {
	out := new(EventsConfiguration)
	err := c.cc.Invoke(ctx, "/myeline.api.v1.Myeline/GetEventsConfiguration", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *myelineClient) UpdateEventsConfiguration(ctx context.Context, in *UpdateEventsConfigurationRequest, opts ...grpc.CallOption) (*EventsConfiguration, error) {
	out := new(EventsConfiguration)
	err := c.cc.Invoke(ctx, "/myeline.api.v1.Myeline/UpdateEventsConfiguration", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// MyelineServer is the server API for Myeline service.
// All implementations must embed UnimplementedMyelineServer
// for forward compatibility
type MyelineServer interface {
	GetVersion(context.Context, *GetVersionRequest) (*Version, error)
	ListProjects(context.Context, *ListProjectsRequest) (*ListProjectsResponse, error)
	GetProject(context.Context, *GetProjectRequest) (*Project, error)
	CreateProject(context.Context, *CreateProjectRequest) (*Project, error)
	UpdateProject(context.Context, *UpdateProjectRequest) (*Project, error)
	DeleteProject(context.Context, *DeleteProjectRequest) (*emptypb.Empty, error)
	ListAssets(context.Context, *ListAssetsRequest) (*ListAssetsResponse, error)
	GetAsset(context.Context, *GetAssetRequest) (*Asset, error)
	RegisterAsset(context.Context, *RegisterAssetRequest) (*Asset, error)
	ListRetentionPolicies(context.Context, *ListRetentionPoliciesRequest) (*ListRetentionPoliciesResponse, error)
	GetRetentionPolicy(context.Context, *GetRetentionPolicyRequest) (*RetentionPolicy, error)
	UpdateRetentionPolicy(context.Context, *UpdateRetentionPolicyRequest) (*RetentionPolicy, error)
	DeleteRetentionPolicy(context.Context, *DeleteRetentionPolicyRequest) (*emptypb.Empty, error)
	ListEvents(context.Context, *ListEventsRequest) (*ListEventsResponse, error)
	GetEventsConfiguration(context.Context, *GetEventsConfigurationRequest) (*EventsConfiguration, error)
	UpdateEventsConfiguration(context.Context, *UpdateEventsConfigurationRequest) (*EventsConfiguration, error)
	mustEmbedUnimplementedMyelineServer()
}

// UnimplementedMyelineServer must be embedded to have forward compatible implementations.
type UnimplementedMyelineServer struct {
}

func (UnimplementedMyelineServer) GetVersion(context.Context, *GetVersionRequest) (*Version, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetVersion not implemented")
}
func (UnimplementedMyelineServer) ListProjects(context.Context, *ListProjectsRequest) (*ListProjectsResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method ListProjects not implemented")
}
func (UnimplementedMyelineServer) GetProject(context.Context, *GetProjectRequest) (*Project, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetProject not implemented")
}
func (UnimplementedMyelineServer) CreateProject(context.Context, *CreateProjectRequest) (*Project, error) {
	return nil, status.Errorf(codes.Unimplemented, "method CreateProject not implemented")
}
func (UnimplementedMyelineServer) UpdateProject(context.Context, *UpdateProjectRequest) (*Project, error) {
	return nil, status.Errorf(codes.Unimplemented, "method UpdateProject not implemented")
}
func (UnimplementedMyelineServer) DeleteProject(context.Context, *DeleteProjectRequest) (*emptypb.Empty, error) {
	return nil, status.Errorf(codes.Unimplemented, "method DeleteProject not implemented")
}
func (UnimplementedMyelineServer) ListAssets(context.Context, *ListAssetsRequest) (*ListAssetsResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method ListAssets not implemented")
}
func (UnimplementedMyelineServer) GetAsset(context.Context, *GetAssetRequest) (*Asset, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetAsset not implemented")
}
func (UnimplementedMyelineServer) RegisterAsset(context.Context, *RegisterAssetRequest) (*Asset, error) {
	return nil, status.Errorf(codes.Unimplemented, "method RegisterAsset not implemented")
}
func (UnimplementedMyelineServer) ListRetentionPolicies(context.Context, *ListRetentionPoliciesRequest) (*ListRetentionPoliciesResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method ListRetentionPolicies not implemented")
}
func (UnimplementedMyelineServer) GetRetentionPolicy(context.Context, *GetRetentionPolicyRequest) (*RetentionPolicy, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetRetentionPolicy not implemented")
}
func (UnimplementedMyelineServer) UpdateRetentionPolicy(context.Context, *UpdateRetentionPolicyRequest) (*RetentionPolicy, error) {
	return nil, status.Errorf(codes.Unimplemented, "method UpdateRetentionPolicy not implemented")
}
func (UnimplementedMyelineServer) DeleteRetentionPolicy(context.Context, *DeleteRetentionPolicyRequest) (*emptypb.Empty, error) {
	return nil, status.Errorf(codes.Unimplemented, "method DeleteRetentionPolicy not implemented")
}
func (UnimplementedMyelineServer) ListEvents(context.Context, *ListEventsRequest) (*ListEventsResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method ListEvents not implemented")
}
func (UnimplementedMyelineServer) GetEventsConfiguration(context.Context, *GetEventsConfigurationRequest) (*EventsConfiguration, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetEventsConfiguration not implemented")
}
func (UnimplementedMyelineServer) UpdateEventsConfiguration(context.Context, *UpdateEventsConfigurationRequest) (*EventsConfiguration, error) {
	return nil, status.Errorf(codes.Unimplemented, "method UpdateEventsConfiguration not implemented")
}
func (UnimplementedMyelineServer) mustEmbedUnimplementedMyelineServer() {}

// UnsafeMyelineServer may be embedded to opt out of forward compatibility for this service.
// Use of this interface is not recommended, as added methods to MyelineServer will
// result in compilation errors.
type UnsafeMyelineServer interface {
	mustEmbedUnimplementedMyelineServer()
}

func RegisterMyelineServer(s grpc.ServiceRegistrar, srv MyelineServer) {
	s.RegisterService(&Myeline_ServiceDesc, srv)
}

func _Myeline_GetVersion_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(GetVersionRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(MyelineServer).GetVersion(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/myeline.api.v1.Myeline/GetVersion",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(MyelineServer).GetVersion(ctx, req.(*GetVersionRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _Myeline_ListProjects_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(ListProjectsRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(MyelineServer).ListProjects(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/myeline.api.v1.Myeline/ListProjects",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(MyelineServer).ListProjects(ctx, req.(*ListProjectsRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _Myeline_GetProject_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(GetProjectRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(MyelineServer).GetProject(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/myeline.api.v1.Myeline/GetProject",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(MyelineServer).GetProject(ctx, req.(*GetProjectRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _Myeline_CreateProject_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(CreateProjectRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(MyelineServer).CreateProject(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/myeline.api.v1.Myeline/CreateProject",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(MyelineServer).CreateProject(ctx, req.(*CreateProjectRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _Myeline_UpdateProject_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(UpdateProjectRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(MyelineServer).UpdateProject(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/myeline.api.v1.Myeline/UpdateProject",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(MyelineServer).UpdateProject(ctx, req.(*UpdateProjectRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _Myeline_DeleteProject_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(DeleteProjectRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(MyelineServer).DeleteProject(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/myeline.api.v1.Myeline/DeleteProject",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(MyelineServer).DeleteProject(ctx, req.(*DeleteProjectRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _Myeline_ListAssets_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(ListAssetsRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(MyelineServer).ListAssets(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/myeline.api.v1.Myeline/ListAssets",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(MyelineServer).ListAssets(ctx, req.(*ListAssetsRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _Myeline_GetAsset_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(GetAssetRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(MyelineServer).GetAsset(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/myeline.api.v1.Myeline/GetAsset",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(MyelineServer).GetAsset(ctx, req.(*GetAssetRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _Myeline_RegisterAsset_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(RegisterAssetRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(MyelineServer).RegisterAsset(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/myeline.api.v1.Myeline/RegisterAsset",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(MyelineServer).RegisterAsset(ctx, req.(*RegisterAssetRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _Myeline_ListRetentionPolicies_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(ListRetentionPoliciesRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(MyelineServer).ListRetentionPolicies(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/myeline.api.v1.Myeline/ListRetentionPolicies",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(MyelineServer).ListRetentionPolicies(ctx, req.(*ListRetentionPoliciesRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _Myeline_GetRetentionPolicy_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(GetRetentionPolicyRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(MyelineServer).GetRetentionPolicy(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/myeline.api.v1.Myeline/GetRetentionPolicy",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(MyelineServer).GetRetentionPolicy(ctx, req.(*GetRetentionPolicyRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _Myeline_UpdateRetentionPolicy_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(UpdateRetentionPolicyRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(MyelineServer).UpdateRetentionPolicy(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/myeline.api.v1.Myeline/UpdateRetentionPolicy",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(MyelineServer).UpdateRetentionPolicy(ctx, req.(*UpdateRetentionPolicyRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _Myeline_DeleteRetentionPolicy_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(DeleteRetentionPolicyRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(MyelineServer).DeleteRetentionPolicy(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/myeline.api.v1.Myeline/DeleteRetentionPolicy",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(MyelineServer).DeleteRetentionPolicy(ctx, req.(*DeleteRetentionPolicyRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _Myeline_ListEvents_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(ListEventsRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(MyelineServer).ListEvents(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/myeline.api.v1.Myeline/ListEvents",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(MyelineServer).ListEvents(ctx, req.(*ListEventsRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _Myeline_GetEventsConfiguration_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(GetEventsConfigurationRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(MyelineServer).GetEventsConfiguration(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/myeline.api.v1.Myeline/GetEventsConfiguration",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(MyelineServer).GetEventsConfiguration(ctx, req.(*GetEventsConfigurationRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _Myeline_UpdateEventsConfiguration_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(UpdateEventsConfigurationRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(MyelineServer).UpdateEventsConfiguration(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/myeline.api.v1.Myeline/UpdateEventsConfiguration",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(MyelineServer).UpdateEventsConfiguration(ctx, req.(*UpdateEventsConfigurationRequest))
	}
	return interceptor(ctx, in, info, handler)
}

// Myeline_ServiceDesc is the grpc.ServiceDesc for Myeline service.
// It's only intended for direct use with grpc.RegisterService,
// and not to be introspected or modified (even as a copy)
var Myeline_ServiceDesc = grpc.ServiceDesc{
	ServiceName: "myeline.api.v1.Myeline",
	HandlerType: (*MyelineServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "GetVersion",
			Handler:    _Myeline_GetVersion_Handler,
		},
		{
			MethodName: "ListProjects",
			Handler:    _Myeline_ListProjects_Handler,
		},
		{
			MethodName: "GetProject",
			Handler:    _Myeline_GetProject_Handler,
		},
		{
			MethodName: "CreateProject",
			Handler:    _Myeline_CreateProject_Handler,
		},
		{
			MethodName: "UpdateProject",
			Handler:    _Myeline_UpdateProject_Handler,
		},
		{
			MethodName: "DeleteProject",
			Handler:    _Myeline_DeleteProject_Handler,
		},
		{
			MethodName: "ListAssets",
			Handler:    _Myeline_ListAssets_Handler,
		},
		{
			MethodName: "GetAsset",
			Handler:    _Myeline_GetAsset_Handler,
		},
		{
			MethodName: "RegisterAsset",
			Handler:    _Myeline_RegisterAsset_Handler,
		},
		{
			MethodName: "ListRetentionPolicies",
			Handler:    _Myeline_ListRetentionPolicies_Handler,
		},
		{
			MethodName: "GetRetentionPolicy",
			Handler:    _Myeline_GetRetentionPolicy_Handler,
		},
		{
			MethodName: "UpdateRetentionPolicy",
			Handler:    _Myeline_UpdateRetentionPolicy_Handler,
		},
		{
			MethodName: "DeleteRetentionPolicy",
			Handler:    _Myeline_DeleteRetentionPolicy_Handler,
		},
		{
			MethodName: "ListEvents",
			Handler:    _Myeline_ListEvents_Handler,
		},
		{
			MethodName: "GetEventsConfiguration",
			Handler:    _Myeline_GetEventsConfiguration_Handler,
		},
		{
			MethodName: "UpdateEventsConfiguration",
			Handler:    _Myeline_UpdateEventsConfiguration_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "api.proto",
}
